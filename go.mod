module gitlab.com/Spouk/work-ssh-com

go 1.16

require (
	github.com/olekukonko/tablewriter v0.0.5
	gitlab.com/Spouk/gotool v0.0.0-20210814190327-15c61b74cc1e
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
	gopkg.in/yaml.v2 v2.4.0
)
