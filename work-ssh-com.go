package main

import (
	"bufio"
	"flag"
	"github.com/olekukonko/tablewriter"
	"gitlab.com/Spouk/gotool/config"
	"golang.org/x/crypto/ssh"
	"gopkg.in/yaml.v2"
	"log"
	"net"
	"os"
	"strings"
	"sync"
	"time"
)

const logprefix = "[work-apcups-checker] "
const logflags = log.LstdFlags | log.Lshortfile

type ConfigStruct struct {
	Debug    bool          `yaml:"debug"`
	Timeout  time.Duration `yaml:"timeout"`
	UserBase string        `yaml:"userbase"`
	PassBase string        `yaml:"passbase"`
	Servers  []*server     `yaml:"servers"`
}
type server struct {
	IP       string   `yaml:"ip"`
	Port     string   `yaml:"sshport"`
	User     string   `yaml:"sshuser"`
	Password string   `yaml:"sshpassword"`
	Commands []string `yaml:"commands"`
}

type checkResult struct {
	IP           string
	UpsilonDir   string
	ApcupsDir    string
	UpsilonDemon string
	ApcupsDemon  string
}

type WorkApcups struct {
	log    *log.Logger
	cfg    *ConfigStruct
	Result []*checkResult
	Lock   sync.RWMutex
	TW     *tablewriter.Table
}

var (
	user        string
	pass        string
	configParam string
	cli         bool //флаг показывающий,что использовать данные из параметров консоли а не из конфига для авторизации на удаленных машинах
	generator   bool
	genfile     string
	comm string
	comm2 string
	comm3 string
)

func main() {
	flag.BoolVar(&generator, "generator", false, "флаг генерации конфига из списка серверов")
	flag.StringVar(&comm, "com", "", "первая команда")
	flag.StringVar(&comm2, "com2", "", " команда")
	flag.StringVar(&comm3, "com3", "", " команда")
	flag.StringVar(&genfile, "genfile", "", "файл со списком серверов в формате 127.0.0.1:22")
	flag.StringVar(&user, "user", "", "базовый пользователь под которым производить подключения к удаленным машинам")
	flag.StringVar(&pass, "pass", "", "пароль пользователя под которым производится операции на удаленной машине")
	flag.StringVar(&configParam, "config", "", "файл конфигурации с полным путем")
	flag.BoolVar(&cli, "cli", false, "флаг, определяющий откуда брать данные пользователя под которым проходит добавление,консоль или конфиг")
	flag.Parse()

	//генератор
	if generator && genfile != "" {
		m := NewWorkApcups(configParam, true)
		m.GeneratorConfig(genfile)
		os.Exit(1)
	}

	//добавление с данными из консоли
	if cli && configParam != "" && user != "" && pass != "" {
		m := NewWorkApcups(configParam, false)
		m.log.Println("=> [user mode] ")

		sw := &sync.WaitGroup{}
		for _, x := range m.cfg.Servers {
			sw.Add(1)
			x.User = user
			x.Password = pass
			go m.Checkhost(x, sw)
		}
		sw.Wait()
		m.log.Println("завершение работы")

		//добавление с данными из конфига
	} else if cli == false && configParam != "" {
		m := NewWorkApcups(configParam, false)
		m.log.Println("==> [config mode]")

		sw := &sync.WaitGroup{}
		for _, x := range m.cfg.Servers {
			sw.Add(1)
			go m.Checkhost(x, sw)
		}



		sw.Wait()
		m.log.Println("завершение работы")

	} else {
		flag.PrintDefaults()
		os.Exit(1)
	}
}
func NewWorkApcups(fconfig string, emptyGen bool) *WorkApcups {
	w := &WorkApcups{
		log: log.New(os.Stdout, logprefix, logflags),
	}
	if emptyGen {
		return w
	}
	tmpcfg := &ConfigStruct{}
	cc := config.NewConf("/tmp/", os.Stdout)
	err := cc.ReadConfig(fconfig, tmpcfg)
	if err != nil {
		w.log.Panic(err)
	}
	w.cfg = tmpcfg
	return w
}
func (w *WorkApcups) exec(c *ssh.Client, command string) string {
	cc, err := c.NewSession()
	if err != nil {
		w.log.Printf("Ошибка создания сессии: %v\n", err)
	}

	res, err := cc.CombinedOutput(command)
	if err != nil {
		log.Printf("[%s] Ошибка выполнения команды:  %s\n", err, string(res))
	}
	return string(res)
}
func (w *WorkApcups) connectssh(s *server) (*ssh.Client, error) {
	w.log.Printf("========[%v] %s %s %s\n", s.IP, s.Port, s.User, s.Password)
	cc := &ssh.ClientConfig{
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		User:            s.User,
		Auth: []ssh.AuthMethod{
			ssh.Password(s.Password),
		},
		Timeout: time.Second * w.cfg.Timeout,
	}
	w.log.Printf("CC: %v\n",cc)
	connection, err := ssh.Dial("tcp", net.JoinHostPort(s.IP, s.Port), cc)
	if err != nil {
		return nil, err
	}
	return connection, nil
}
func (w *WorkApcups) Checkhost(s *server, sw *sync.WaitGroup) {
	w.log.Println(s)
	defer sw.Done()
	res := &checkResult{}
	res.IP = s.IP
	con, err := w.connectssh(s)
	if err != nil {
		w.log.Printf("[running - `%v`] %v\n", s.IP, err)
		return
	}
	for _,  v := range s.Commands {
		result := w.exec(con, v)
		w.log.Println(result)

	}
	w.Lock.Lock()
	w.Result = append(w.Result, res)
	w.Lock.Unlock()
}

func (w *WorkApcups) GeneratorConfig(inFile string) {
	f, err := os.OpenFile(inFile, os.O_RDWR, os.ModePerm)
	if err != nil {
		w.log.Panic(err)
	}
	defer f.Close()
	sc := bufio.NewScanner(f)
	stock := ConfigStruct{}
	for sc.Scan() {
		pp := strings.Split(sc.Text(), ":")
		s := server{
			IP:       pp[0],
			Port:     pp[1],
			Commands: []string{comm, comm2, comm3},
			User: user,
			Password: pass,
		}
		com := []string{}
		s.Commands = com
		stock.Servers = append(stock.Servers, &s)
	}
	enc := yaml.NewEncoder(os.Stdout)
	err = enc.Encode(stock)
	if err != nil {
		w.log.Panic(err)
	}
}
